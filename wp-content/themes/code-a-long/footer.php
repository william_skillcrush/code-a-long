<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package code-a-long
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-name">
			<h3><?php bloginfo( 'name' ); ?></h3>
		</div>
		<div class="site-address">
			<p>12345 Cahaba Heights Ct, Ste 148, Birmingham, Alabama</p>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
